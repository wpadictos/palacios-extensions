<?php
/*
Plugin Name: Palacios Online DIVI Custom Modules
Plugin URI:  https://palacios-onlinede[3~de
Description: Custom modules for DIVI Builder
Version:     1.0.0
Author:      Jose Rodriguez
Author URI:  https://palacios-online.de
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: podm-palacios-extensions
Domain Path: /languages

Palacios Online DIVI Custom Modules is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Palacios Online DIVI Custom Modules is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Palacios Online DIVI Custom Modules. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
*/


if ( ! function_exists( 'podm_initialize_extension' ) ):
/**
 * Creates the extension's main class instance.
 *
 * @since 1.0.0
 */
function podm_initialize_extension() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/PalaciosExtensions.php';
}
add_action( 'divi_extensions_init', 'podm_initialize_extension' );

function pe_add_modules_scripts(){
    wp_enqueue_style(
        'po-modules-style',
        plugins_url() . '/palacios-extensions/styles/pe-custom-modules.css',
        NULL,
        NULL
    );
}
add_action( 'wp_enqueue_scripts', 'pe_add_modules_scripts' );


endif;
