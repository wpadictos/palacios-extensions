// External Dependencies
import React, { Component, Fragment } from 'react';

// Internal Dependencies
import './style.css';


class MyraButton extends Component {

    static slug = 'podm_myra_button';

render() {
    //const Content = this.props.content;

    return (
        <Fragment>
            <div class="et_pb_myra_button">
                <a href="#" class="myra-button">{this.props.butonText}</a>
            </div>
        </Fragment>
    );
}
}

export default MyraButton;
