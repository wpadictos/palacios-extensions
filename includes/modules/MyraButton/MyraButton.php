<?php

class PODM_MyraButton extends ET_Builder_Module {

    public $slug       = 'podm_myra_button';
    public $vb_support = 'on';

    protected $module_credits = array(
        'module_uri' => 'https://palacios-online.de',
        'author'     => 'jmrpadrino',
        'author_uri' => 'https://palacios-online.de',
    );

    public function init() {
        $this->name = esc_html__( 'Myra Button', 'podm-palacios-extensions' );
    }

    public function get_fields() {
        return array(
            'heading'     => array(
                'label'           => esc_html__( 'Button Text', 'simp-simple-extension' ),
                'type'            => 'text',
                'option_category' => 'basic_option',
                'description'     => esc_html__( 'Input your desired heading here.', 'simp-simple-extension' ),
                'toggle_slug'     => 'main_content',
            ),
            'myra_button_url' => array(
                'label'           => 'Myra Button URL',
                'type'            => 'text',
                'toggle_slug'     => 'link',
            ),
/*            'myra_blurb_button_text' => array(
                'label'           => 'Button text',
                'type'            => 'text',
                'toggle_slug'     => 'button',
            ),*/

            'myra_button_show_button_opennewtab' => array(
                'label'           => 'Open on new tab?',
                'type'            => 'yes_no_button',
                'options'         => array(
                    'off' => esc_html__( 'No', 'et_builder' ),
                    'on'  => esc_html__( 'Yes', 'et_builder' ),
                ),
                'toggle_slug'     => 'button',
            ),
        );
    }


    public function render( $attrs, $content = null, $render_slug ) {

        $open_on_new_tab = '';

        if ( 'on' == $this->props['myra_button_show_button_opennewtab'] ){
            $open_on_new_tab = ' target="_blank"';
        }

        return sprintf(
            '<a href="%2$s" %3$s class="et_pb_myra_button"><span class="myra-button-text">%1$s</span><span class="myra-button-arrow"></span></a>
            ',
            esc_html( $this->props['heading'] ), // 1 - Title
            $this->props['myra_button_url'], // 2 - URL
            $open_on_new_tab
        );

    }
}

new PODM_MyraButton;
