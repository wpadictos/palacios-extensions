import MyraBlurb from './MyraBlurb/MyraBlurb';
import MyraButton from './MyraButton/MyraButton';

export default [
    MyraBlurb,
    MyraButton
];
