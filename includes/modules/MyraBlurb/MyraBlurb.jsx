// External Dependencies
import React, { Component, Fragment } from 'react';

// Internal Dependencies
import './style.css';


class MyraBlurb extends Component {

    static slug = 'podm_myra_blurb';

render() {
    //const Content = this.props.content;

    return (
        <Fragment>
            <div class="et_pb_myra_blurb">
                <img src="#" title="%1$s" />
                <h1 className="simp-simple-header-heading">{this.props.heading}</h1>
                <p>{this.props.content()}
                </p>
            </div>
        </Fragment>
    );
}
}

export default MyraBlurb;
