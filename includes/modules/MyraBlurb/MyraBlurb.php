<?php

class PODM_MyraBlurb extends ET_Builder_Module {

    public $slug       = 'podm_myra_blurb';
    public $vb_support = 'on';

    protected $module_credits = array(
        'module_uri' => 'https://palacios-online.de',
        'author'     => '<jmrpadrino />',
        'author_uri' => 'https://palacios-online.de',
    );

    public function init() {
        $this->name = esc_html__( 'Myra Blurb', 'podm-palacios-extensions' );
    }

    public function get_fields() {
        return array(
            'heading'     => array(
                'label'           => esc_html__( 'Heading', 'simp-simple-extension' ),
                'type'            => 'text',
                'option_category' => 'basic_option',
                'description'     => esc_html__( 'Input your desired heading here.', 'simp-simple-extension' ),
                'toggle_slug'     => 'main_content',
            ),
            'content' => array(
                'label'           => esc_html__( 'Content', 'podm-palacios-extensions' ),
                'type'            => 'tiny_mce',
                'option_category' => 'basic_option',
                'description'     => esc_html__( 'Content entered here will appear inside the module.', 'podm-palacios-extensions' ),
                'toggle_slug'     => 'main_content',
            ),
            'myra_blurb_image' => array(
                'label'              => esc_html__( 'Myra Blurb Image', 'et_builder' ),
                'type'               => 'upload',
                'option_category'    => 'basic_option',
                'upload_button_text' => esc_attr__( 'Upload an image', 'et_builder' ),
                'choose_text'        => esc_attr__( 'Choose an Image', 'et_builder' ),
                'update_text'        => esc_attr__( 'Set As Image', 'et_builder' ),
                'depends_show_if'    => 'off',
                'description'        => esc_html__( 'Upload an image to display at the top of your blurb.', 'et_builder' ),
                'toggle_slug'        => 'image',
                'dynamic_content'    => 'image',
            ),
            'myra_blurb_button_url' => array(
                'label'           => 'Myra Button URL',
                'type'            => 'text',
                'toggle_slug'     => 'link',
            ),
            'myra_blurb_show_button' => array(
                'label'           => 'Myra Blurb Show link button on frontend',
                'type'            => 'yes_no_button',
                'options'         => array(
                    'off' => esc_html__( 'No', 'et_builder' ),
                    'on'  => esc_html__( 'Yes', 'et_builder' ),
                ),
                'toggle_slug'     => 'button',
            ),
            'myra_blurb_button_text' => array(
                'label'           => 'Button text',
                'type'            => 'text',
                'toggle_slug'     => 'button',
            ),
            'myra_blurb_show_button_opennewtab' => array(
                'label'           => 'Open on new tab?',
                'type'            => 'yes_no_button',
                'options'         => array(
                    'off' => esc_html__( 'No', 'et_builder' ),
                    'on'  => esc_html__( 'Yes', 'et_builder' ),
                ),
                'toggle_slug'     => 'button',
            ),
        );
    }
    

    public function render( $attrs, $content = null, $render_slug ) {
        
        //return '<pre>' . var_dump($this->props) . '</pre>';
                
        $show_blurb_image = '';
        $show_button_html = '';
        $open_on_new_tab = '';
        if ( '' != $this->props['myra_blurb_image'] ){
            $show_blurb_image = sprintf(
                '<div class="et_pb_myra_blurb_image">
                    <img src="%2$s" title="%1$s">
                </div>',
                esc_html( $this->props['heading'] ), // 1 - Alt Attribute
                esc_html( $this->props['myra_blurb_image'] ) // 2 - Image URL
            );
        }
        if ( 'on' == $this->props['myra_blurb_show_button_opennewtab'] ){
            $open_on_new_tab = ' target="_blank"';
        }
        if ( 'on' == $this->props['myra_blurb_show_button'] ){
            $show_button_html = sprintf(
                '<div class="et_pb_myra_blurb_link">
                    <a href="%1$s"%3$s>
                        <div class="myra-blurb-button">
                            <span class="myra-blurb-button-arrow"> </span>
                            <span class="myra-blurb-button-title">%2$s</span>
                        </div>
                    </a>
                </div>',
                $this->props['myra_blurb_button_url'],
                $this->props['myra_blurb_button_text'],
                $open_on_new_tab
            );
        }
        return sprintf(
            '%4$s
            <div class="et_pb_myra_blurb_content">
                <h2 class="et_pb_myra_blurb_heading">%1$s</h2>
                %2$s
            </div>
            %3$s
            ',
            esc_html( $this->props['heading'] ), // 1 - Title
            $this->props['content'], // 2 - Content            
            $show_button_html, // 3 - Button HTML
            $show_blurb_image // 4 - image HTML
        );
        
    }
}

new PODM_MyraBlurb;
